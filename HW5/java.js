// 1. Опишите своими словами, что такое метод обьекта.
//    Метод обьекта - это функции, которые находятся в объекте в качестве его свойств.

"use strict";

function createNewUser(newUser) {
    newUser = {
        firstName: prompt("Type your name"),
        lastName: prompt("Type your surname"),
        birthday: prompt("Type your date of birth", "dd.mm.yyyy"),
        getLogin() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge: function calcYear() {
            const date = new Date();
            const birthdayYear = this.birthday.slice(6);
            return date.getFullYear() - this.birthday.slice(6)
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6)
        }
    };
    Object.defineProperties(newUser, {
        'firstName': {
        writable: false
        },
        'lastName': {
        writable: false
        },
    });
    console.log(newUser.getLogin());
    console.log(`Вам ${newUser.getAge()} лет`);
    console.log(newUser.getPassword());
    return newUser;
    }
createNewUser();