
"use strict";

const tabsTitle = document.querySelectorAll('.tabs-item');
// console.log(tabsTitle);
const tabsItems = document.querySelectorAll('.tabs-block');
// console.log(tabsItems)
// console.log(tab1)
// console.log(document.querySelectorAll('.tabsItem'));
tabsTitle.forEach(function (item) {
   item.addEventListener('click', function () {
      let currentTab = item;
      let tabId = currentTab.getAttribute('data-tab');
      let activeTab = document.querySelector(tabId);
      // console.log(tabId);
      // console.log(activeTab);
      tabsTitle.forEach(function (item) {
         item.classList.remove('active');
      });
      tabsItems.forEach(function (item) {
         item.classList.remove('active');
      });
      currentTab.classList.add('active');
   });
});

const filterBox = document.querySelectorAll('.box');
document.querySelector('nav').addEventListener('click', (event) => {
    if (event.target.tagName !== 'LI') return false;
    let filterClass = event.target.dataset['f'];
    filterBox.forEach(elem => {
        elem.classList.remove('hide');
        if (!elem.classList.contains(filterClass) && filterClass !== 'all') {
            elem.classList.add('hide');
        }
    });

});

const amazingWork = document.querySelectorAll('.filter-item');
amazingWork.forEach(function (item) {
   item.addEventListener('click', function () {
      let currentTab = item;
      let tabId = currentTab.getAttribute('data-f');
      let activeTab = document.querySelector(tabId);
      // console.log(tabId);
      // console.log(activeTab);
      amazingWork.forEach(function (item) {
         item.classList.remove('active');
      });
      currentTab.classList.add('active');
   });
});

const morePictures = document.querySelector('.more-pictures');
const buttonHide = document.querySelector('.filter-btn');
buttonHide.onclick = () => {
      morePictures.style.display = 'grid';
      buttonHide.style.display = 'none'
}
   
const carouselImages = document.querySelector('people-photo');
const clickButton = document.getElementById('click');
console.log(clickButton)

// window.addEventListener('load', () => {


//    const left = document.querySelector('.previous');
//    const right = document.querySelector('.next');
//    const slider = document.querySelector('.people-carousel');
//    const images = document.querySelectorAll('.people-carousel img');
//    console.log(images.style.margin)

//    let counter = 0;
//    const stepSize = images[0].clientWidth;

//    right.addEventListener('click', () => {
//       counter++;
//       images.style.margin = "6px 9px 22px 9px";
//    });

// });
// const left = document.querySelector('.previous');
//    const right = document.querySelector('.next');
//    const slider = document.querySelector('.people-carousel');
// const images = document.querySelectorAll('.people-carousel img');
// const photo = document.querySelector('.people-photo');
// console.log(photo)
// photo.addEventListener('click', (event) => {
//    photo.style.margin = "6px 9px 22px 9px;"
// })
// console.log(events)
// const tabsTitl = document.querySelectorAll('.photo-link');
// // console.log(tabsTitle);
// const tabsItem = document.querySelectorAll('.people-descr');
// // console.log(tabsItems)
// // console.log(tab1)
// // console.log(document.querySelectorAll('.tabsItem'));
// tabsTitl.forEach(function (item) {
//    item.addEventListener('click', function () {
//       let currentTab = item;
//       let tabId = currentTab.getAttribute('data-p');
//       let activeTab = document.querySelector(tabId);
//       // console.log(tabId);
//       // console.log(activeTab);
//       tabsTitl.forEach(function (item) {
//          item.classList.remove('active');
//       });
//       tabsItem.forEach(function (item) {
//          item.classList.remove('active');
//       });
//       currentTab.classList.add('active');
//    });
// });
const arrows = document.getElementById('arrows');
console.log(arrows)
const prev = document.querySelector('.previous');
const next = document.querySelector('.next');
const slides = document.querySelectorAll('.people-photo-wrappper');
console.log(slides)

let index = 0;

const activeSlide = n => {
   console.log(n);
   let slide = document.querySelectorAll('.people-photo-wrappper');
   for (slide of slides) {
      slide.classList.remove('active');
   }
   slides[n].classList.add('active');
}

const nextSlide = () => {
   if(index == slides.length - 1) {
      index = 0;
      activeSlide(index);
   } else {
      index++;
      activeSlide(index);
   }
}

const prevSlide = () => {
   if(index == 0) {
      index = slides.length - 1;
      activeSlide(index);
   } else {
      index--;
      activeSlide(index);
   }
}

next.addEventListener('click', nextSlide);
prev.addEventListener('click', prevSlide);
// arrows.addEventListener('click', function() {
//    let target = event.target;
//    let slideIndex = 0;
//    if (target === next) {
//       slideIndex++;
//    } else if (target === prev) {
//       slideIndex--;
//    } else {
//       return;
//    }
// })
