// 1.Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// DOM - это объектная модель документа, которую браузер создаёт в памяти компьютера на основании HTML-кода, полученного им от сервера.

// "use strict";

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// const arr2 = ["1", "2", "3", "sea", "user", 23];
// const arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function createList(arr, list) {
    list = document.createElement('ul');
    let newArray = arr.map(item => {
        let listItem = document.createElement('li');
        listItem.appendChild(document.createTextNode(item));
        list.appendChild(listItem);
    });
    return list;
};
document.getElementById("root").appendChild(createList(arr));