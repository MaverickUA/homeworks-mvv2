
"use strict";

const images = document.querySelectorAll('.images-wrapper img')
console.log(images);
let i = 0;
function nextImg() {
  if (i === images.length - 1) {
    images[i].style.display = 'none';
    i = 0;
    images[0].style.display = 'block';
  } else {
    images[i].style.display = 'none';
    images[i + 1].style.display = 'block';
    i++;
  }
}
let timerId = setInterval(nextImg, 3000)
document.getElementById('btn-stop').addEventListener('click', () => {
  clearInterval(timerId);
});
document.getElementById('btn-resume').addEventListener('click', () => {
  setInterval(nextImg, 3000);
});
