**1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()**

- setTimeout() дозволяє викликати функцію один раз через певний інтервал часу.
- setInterval() дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.

---

**2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?**

- Виклик функції відбудеться настільки швидко, наскільки це можливо (без затримки), але тільки після виконання поточного коду.

---

**3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?**

- Для того, щоб функція та змінні функції не займали пам'ять.

---
