// 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
// События клавиатуры предназначены именно для работы с клавиатурой. 
// Их можно использовать для проверки ввода в <input>, но будут недочёты. 
// Например, текст может быть вставлен мышкой, при помощи правого клика и меню, без единого нажатия клавиши. 
// Тоже самое и с вводом текста в мобильных устройствах, они также не генерируют keypress/keydown, 
// а сразу вставляют текст в поле. Обработать ввод на них при помощи клавиатурных событий нельзя.
"use strict";

const btnList = document.querySelectorAll('.btn');
console.log(btnList);
const btn = document.querySelector('.btn')
document.addEventListener('keypress', function (i) {
   console.log(i)
});
btnList.forEach(function(item) {
   console.log(item.innerHTML)
   document.addEventListener('keypress', function(e) {
      if (e.key == item.innerHTML){
      item.style.backgroundColor = 'blue'
      }
      else {
      item.style.backgroundColor = 'black'
      }
   });
   console.log(item)
});