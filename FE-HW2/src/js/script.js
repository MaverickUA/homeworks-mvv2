
"use strict";

const btn = document.querySelector('.header__btn-wrapper');
const headerList = document.querySelector('.header__list');
btn.addEventListener('click', function (event) {
    btn.classList.toggle('active');
    headerList.classList.toggle('active');
});

