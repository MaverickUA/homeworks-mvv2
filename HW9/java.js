// 1.Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Обработчик событий - это функция, которая обрабатывает, или откликается на событие. 

"use strict";

const tabsTitle = document.querySelectorAll('.tabs-title');
// console.log(tabsTitle);
const tabsItems = document.querySelectorAll('.tabsItem');
// console.log(tabsItems)
// console.log(tab1)
// console.log(document.querySelectorAll('.tabsItem'));
tabsTitle.forEach(function (item) {
   item.addEventListener('click', function () {
      let currentTab = item;
      let tabId = currentTab.getAttribute('data-tab');
      let activeTab = document.querySelector(tabId);
      
      // console.log(tabId);
      // console.log(activeTab);
      tabsTitle.forEach(function (item) {
         item.classList.remove('active');
      });
      tabsItems.forEach(function (item) {
         item.classList.remove('active');
      });
      currentTab.classList.add('active');
      activeTab.classList.add('active');
   });
});
