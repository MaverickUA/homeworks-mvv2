// 1. Описать своими словами для чего вообще нужны функции в программировании.
//    Функции нужны, чтобы заметно упрощать и сокращать код, адаптировать его для разных платформ,
//    делать более отказоустойчивым, легко отлаживать. И вообще порядок в функциях — порядок в голове.
// 2. Описать своими словами, зачем в функцию передавать аргумент.
//    Чтобы параметрам функции задать реальные значения. Для этого параметрам функции передают аргументы (реальные значения).

let num1;
let num2;
let action;

do {
    num1 = prompt("Enter first number");
    num1 = (num1 && !isNaN(num1) && typeof +num1 === "number");
    num2 = prompt("Enter second number");
    action = prompt("Enter action (+, -, /, * or **)");
} while (
    !(num1 && !isNaN(num1) && typeof +num1 === "number") &&
    !(num2 && !isNaN(num2) && typeof +num2 === "number") &&
    action !== "+" &&
    action !== "-" &&
    action !== "/" &&
    action !== "**" &&
    action !== "*");
if (num1 === false) {
    prompt("Enter first number");
}
let res;
switch (action) {
    case "+":
        res = +num1 + +num2;
        break;
    case "*":
        res = num1 * num2;
        break;
    case "/":
        res = num1 / num2;
        break;
    case "-":
        res = num1 - num2;
        break;
    case "**":
        res = num1 ** num2;
        break;
}       
console.log(
    `Над числами ${num1} и ${num2} была произведена операция '${action}'. Результат: ${res}`
);


    