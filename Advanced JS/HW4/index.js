"use strict";

const root = document.getElementById('root');
const URI = "https://ajax.test-danit.com/api/swapi/";

class Films {
    constructor(url, entity, root) {
        this.url = url;
        this.entity = entity;
        this.root = root;
    }

    request(url = this.url, entity = "", id = "") {
        return new Promise((resolve, reject) => {
 
            const xhr = new XMLHttpRequest();
            xhr.open("GET", `${url}${entity}${id}`);
            xhr.send();
        
            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(e);
        });
    }
    

    render(url = this.url, entity = "", id = "") {
        const list = document.createElement("ul");
        this.request(this.url, this.entity)
            .then((response) => {
                const data = JSON.parse(response);
                const items = data.map(({
                    episodeId,
                    name,
                    openingCrawl,
                    characters }) => {
                    const listItem = document.createElement("li");
                    const filmInfo = document.createElement("span");
                    const filmOpCrawl = document.createElement("p");

                    filmInfo.innerText = `Episode ${episodeId}:  "${name}"`;
                    filmOpCrawl.innerText = `${openingCrawl}`;
                  
                    listItem.append(filmInfo);
                    listItem.append(filmOpCrawl);
                    
                    const charactersInfo = characters.map((character) =>
                        this.request(character)
                    );

                    Promise.allSettled(charactersInfo).then((data) => {
                        data.forEach(({ value }) => {
                            const name = JSON.parse(value).name;                        
                            const filmCharacters = document.createElement("p");
                            filmCharacters.innerText = `Character: ${name}`;
                             
                            listItem.append(filmCharacters);
                        });
                    });

                    return listItem;
                });

                list.append(...items);
                this.root.append(list);
            })
            .catch((e) => {
                console.log(e);
            });
    }
}
const entity = "films";

const films = new Films(URI, entity, root);

films.render();
