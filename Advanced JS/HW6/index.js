"use strict";

const root = document.getElementById('root');
const URI = "https://api.ipify.org/?format=json";

const ipBtn = document.createElement("button");
ipBtn.id = 'ipBtn';
ipBtn.className = 'ipBtn';
ipBtn.textContent = "Find By IP";
root.append(ipBtn);

const getInfo = async () => {
  const resp = await fetch(URI);
    const data = await resp.json();
    const { ip } = data;
    const adressResp = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`);
    const adressData = await adressResp.json();
    const { continent,
        country,
        regionName,
        city,
        district } = adressData;
    const adressDiv = document.createElement('div');
    adressDiv.id = 'adressDiv';
    adressDiv.className = 'adressDiv';
    adressDiv.innerHTML = `
        <p>Continent: ${continent},</p>
        <p>Country: ${country},</p>
        <p>RegionName: ${regionName},</p>
        <p>City: ${city},</p>
        <p>District ${district}</p>
        `;
    root.append(adressDiv);
}

ipBtn.addEventListener('click', getInfo)
