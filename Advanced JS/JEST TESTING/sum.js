function sum(a, b) {
  return a + b;  
}

module.exports = sum;

console.log(sum(1,2)); // 3
console.log(sum(2,2)); // 4
console.log(sum(3,2)); // 5
console.log(sum("1",2)); // 12
