class Employee {
    constructor(name, age, salary) {
    this._name = name; 
    this._age = age;
    this._salary = salary; 
}
    get name() {
        return this._name;
    }
    set name(value){
        this._name = value;
    } 
    get age() {
        return this._age;
    }
    set age(value){
        this._age = value;
    } 
    get salary() {
        return this._salary;
    }
    set salary(value){
        this._salary = value;
    } 
}

class Programmer extends Employee {
    constructor(lang, ...args){
        super(...args);
        this._lang = lang;
}
    get salary() {
        return this._salary *3;
    }
}

const em = new Employee(this._salary);
const john = new Programmer("english", "John", 34, 3000);
const mike = new Programmer("french", "Mike", 55, 3000);
const susan = new Programmer("ukrainian", "Susan", 21, 3000);

console.log(em);
console.log(john.salary);
console.log(mike.salary);
console.log(susan.salary);
console.log(john);
console.log(mike);
console.log(susan);