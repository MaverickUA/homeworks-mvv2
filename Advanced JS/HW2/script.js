const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

  
function createList(books, ul) {

  let div = document.querySelector('#root')
  ul = document.createElement('ul');
  div.append(ul);
    
  books.forEach(item => {
    let listItem = document.createElement('li');
    let textNodeCreate = document.createTextNode(item.author + ' ' + item.name + ' ' + item.price);
    listItem.appendChild(textNodeCreate);
    try {
      if (!item.author) {
        throw new SyntaxError('ОШИБКА!!! В одном из обьектов отсутствует поле автор')
      }
      else if (!item.name) {
        throw new SyntaxError('ОШИБКА!!! В одном из обьектов отсутствует поле имя')
      }
      else if (!item.price) {
        throw new SyntaxError('ОШИБКА!!! В одном из обьектов отсутствует поле цена')
      }
      ul.appendChild(listItem);
    } catch (e) {
      console.log(e);
    }
  });
};
createList(books);

