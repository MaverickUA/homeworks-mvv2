// 1.Опишите своими словами как работает цикл forEach.
//  Цикл forEach перебирает все элементы массива или выполняет указанную функцию по одному разу для каждого элемента массива.

"use strict";

const arr = ['hello', 'world', 23, '23', null];
const dataType = 'string';

const filterBy = (arr, dataType) =>
    arr.filter((arr) =>
        (typeof arr !== dataType));

console.log(filterBy(arr, dataType));